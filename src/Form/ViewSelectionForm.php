<?php

namespace Drupal\views_diff\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Views;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\InvokeCommand;

/**
 * Class ViewSelectionForm.
 */
class ViewSelectionForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'views_diff_view_selection_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $name_of_current_view = \Drupal::request()->query->get('name_of_current_view');
    $current_view = Views::getView($name_of_current_view);
    $form_state->set('name_of_current_view', $name_of_current_view);
    // Get views names for population of the select list.
    // Fetch only views which have the same base table as the current view has.
    $base_table = $current_view->storage->get('base_table');
    $query = \Drupal::entityQuery('view');
    $query->condition('base_table', $base_table);
    $view_ids = $query->execute();

    $name_of_selected_view = '';

    // The first run (initialization).
    if ($form_state->getValue('select_view') === NULL) {

      $name_of_selected_view = $current_view
        ->storage
        ->getThirdPartySetting('views_diff', 'name_of_view_to_compare_against');

      $selected_display = $current_view
        ->storage
        ->getThirdPartySetting('views_diff', 'display_of_view_to_compare_against');

      // Exclude the current view from the list.
      unset($view_ids[$name_of_current_view]);

      // Check if the selected view still exists.
      if (!array_key_exists($name_of_selected_view, $view_ids)) {
        $name_of_selected_view = '';
      }
    }
    // The case when a user selected a view.
    elseif ($form_state->getValue('select_view') != '') {

      $name_of_selected_view = $form_state->getValue('select_view');
    }

    $form['select_view'] = [
      '#type' => 'select',
      '#title' => $this->t('Select a view to compare against'),
      '#options' => $view_ids,
      '#empty_option' => t('- Select -'),
      '#default_value' => $name_of_selected_view,
      '#ajax' => [
        'callback' => '::ajaxSelectHandler',
        'event' => 'change',
        'wrapper' => 'select-display-ajax-wrapper',
      ],
    ];

    $form['ajax_wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'select-display-ajax-wrapper',
      ],
    ];

    if ($name_of_selected_view) {

      $selected_view = Views::getView($name_of_selected_view);
      // Get all displays of the selected view.
      foreach ($selected_view->storage->get('display') as $key => $value) {
        $displays_of_selected_view[$key] = $key;
      }

      $form['ajax_wrapper']['select_display'] = [
        '#type' => 'select',
        '#title' => $this->t('Select a view display'),
        '#options' => $displays_of_selected_view,
      ];
    }

    $form['save'] = [
      '#type' => 'button',
      '#value' => $this->t('Save'),
      '#ajax' => [
        'callback' => '::ajaxSaveHandler',
        'event' => 'click',
      ],
    ];

    $form['cancel'] = [
      '#type' => 'button',
      '#value' => $this->t('Cancel'),
      '#ajax' => [
        'callback' => '::ajaxCancelHandler',
        'event' => 'click',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxSelectHandler(array &$form, FormStateInterface $form_state) {
    return $form['ajax_wrapper'];
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxSaveHandler(array &$form, FormStateInterface $form_state) {

    $view_name = $form_state->get('name_of_current_view');
    $view = Views::getView($view_name);

    $selected_view_name = $form_state->getValue('select_view');
    $selected_display = $form_state->getValue('select_display');

    if ($selected_view_name) {
      $view->storage->setThirdPartySetting('views_diff', 'name_of_view_to_compare_against', $selected_view_name);
      $view->storage->setThirdPartySetting('views_diff', 'display_of_view_to_compare_against', $selected_display);
    }
    else {
      $view->storage->setThirdPartySetting('views_diff', 'name_of_view_to_compare_against', NULL);
      $view->storage->setThirdPartySetting('views_diff', 'display_of_view_to_compare_against', NULL);
    }

    $view->save();

    $response = new AjaxResponse();
    $response->addCommand(new CloseModalDialogCommand());

    // Replace the link's text with the name of selected view or with 'None'.
    $link_text = $selected_view_name ?: t('None');
    $selector = '#link-for-selection-view-for-comparision';
    $method = 'text';
    $arguments = [$link_text];

    $response->addCommand(new InvokeCommand($selector, $method, $arguments));

    // Simulate a click on the 'Update preview' button to refresh preview.
    $response->addCommand(new InvokeCommand('#preview-submit', 'click'));

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxCancelHandler(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new CloseModalDialogCommand());
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

}
